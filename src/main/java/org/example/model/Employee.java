package org.example.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@Builder
@EqualsAndHashCode

public class Employee {
    private String employeeId;
    private String firstName;
    private String lastName;
    private String email;

    @Override
    public String toString() {
        return
                "employeeId='" + employeeId + '\'' +
                        ", firstName='" + firstName + '\'' +
                        ", lastName='" + lastName + '\'' +
                        ", email='" + email + '\'';

    }
}